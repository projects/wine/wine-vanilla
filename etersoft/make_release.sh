#!/bin/sh

# load common functions, compatible with local and installed script
. /usr/share/eterbuild/eterbuild
load_mod spec etersoft

VERSION="$(sed -e "s|Wine version \(.*\)|\1|g" ../VERSION)"

SPECNAME=wine-vanilla.spec
if [ "$VERSION" = "$(get_version $SPECNAME)" ] ; then
	fatal "version $VERSION is already current"
fi

# set version from VERSION file
set_version $SPECNAME "$VERSION"
set_release $SPECNAME

add_changelog $SPECNAME -e "- new version $VERSION"

# already defined in spec
#export ETERDESTSRPM=/var/ftp/pub/Etersoft/Wine-vanilla/$VERSION/sources

git add $SPECNAME || fatal
gear-commit

rpmpub -r "$VERSION" $SPECNAME || fatal "Can't build SRPMS"
